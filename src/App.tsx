import React from "react";
import { BrowserRouter, Routes, Route, Link } from "react-router-dom";
import Contacts from "./component/Contacts";
import Home from "./component/Home";
import Services from "./component/Services";
import  'bootstrap/dist/css/bootstrap.min.css' ;


function App() {
  return (
    <>
      <h1>React Router app!</h1>
      <nav className="container">
        <Link className="btn btn-primary me-2" to={'/'}>Home</Link>
        <Link className="btn btn-primary me-2" to={'/services'}>Services</Link>
        <Link className="btn btn-primary" to={'/contacts'}>Contacts</Link>
      </nav>
      <Routes>
          <Route path="/" element={<Home/>}/>
          <Route path="/services" element={<Services/>}/>
          <Route path="/contacts" caseSensitive element={<Contacts/>}/>
      </Routes>
    </>
    )
}

export default App;
